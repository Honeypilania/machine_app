
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { NotificationsService } from "angular2-notifications";
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { User } from "../../models/user.model";
@Injectable({
  providedIn: 'root'
})
export class UserService 
{
  user$:Observable<firebase.User>;
  constructor(private afAuth: AngularFireAuth,private router:Router,private _notifications: NotificationsService,private dialog:MatDialog) 
  { 
      this.user$=afAuth.authState;
  }

  login(email: string, password: string) 
  {
      this.afAuth
          .auth
          .signInWithEmailAndPassword(email, password)
          .then(() =>{
                        this._notifications.success("Login Successfully")
                      })
          .then(() =>{
                        this.router.navigateByUrl('/dash')
                      })
          .catch(err => {
                          this._notifications.error(err)
                        });
  }

  logout() 
  {
      this.afAuth
          .auth
          .signOut()
          .then(() => {
                        this.router.navigateByUrl('/home');
                      }).then(()=>{
                        this._notifications.success('Logout Successfully')
                      });
  }
}
