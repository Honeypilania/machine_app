import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from '../../services/users/user.service';
import { AngularFireDatabase } from '@angular/fire/database';
import { DetailsComponent } from '../details/details.component';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent 
{
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );
    date=true;
    data;
  constructor(private db:AngularFireDatabase,private breakpointObserver: BreakpointObserver,private user:UserService) 
  {
      this.db.object('Date').valueChanges().subscribe(arr=>{
        this.data=arr;
        if((new Date(parseInt(this.data))).toLocaleDateString()==(new Date()).toLocaleDateString())
        {
          this.date=true;
        }
        else this.date=false;
      })
  }
  logout()
  {
    this.user.logout();
  }
}
