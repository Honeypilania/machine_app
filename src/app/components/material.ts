import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule ,MatNativeDateModule, NativeDateAdapter, DateAdapter, MAT_NATIVE_DATE_FORMATS} from '@angular/material';
import { MatListModule } from '@angular/material/list';
import { MatBadgeModule } from '@angular/material/badge';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatMenuModule } from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import { MatDatepickerModule } from '@angular/material/datepicker';

const MY_DATE_FORMATS = {
    parse: {
        dateInput: {month: 'short', year: 'numeric', day: 'numeric'}
    },
    display: {
        dateInput: 'input',
        monthYearLabel: {year: 'numeric', month: 'short'},
        dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
        monthYearA11yLabel: {year: 'numeric', month: 'long'},
    }
 };

 export class MyDateAdapter extends NativeDateAdapter {
    format(date: Date, displayFormat: Object): string {
        
            let day = date.getDate();
            let month = date.getMonth() + 1;
            let year = date.getFullYear();
            return this._to2digit(day) + '/' + this._to2digit(month) + '/' + year;
    }
 
    private _to2digit(n: number) {
        return ('00' + n).slice(-2);
    } 
 }

@NgModule
(
  {
    imports: 
            [
                BrowserAnimationsModule,
                MatCardModule,
                MatFormFieldModule,
                MatIconModule,
                MatInputModule,
                MatButtonModule,
                MatListModule,
                MatBadgeModule,
                ReactiveFormsModule,
                MatToolbarModule,
                MatMenuModule,
                MatDialogModule,
                MatGridListModule,
                MatTableModule,
                FormsModule,
                MatDatepickerModule,
                MatNativeDateModule
            ],
    exports: 
            [
                BrowserAnimationsModule,
                MatCardModule,
                MatFormFieldModule,
                MatIconModule,
                MatInputModule,
                MatButtonModule,
                MatListModule,
                MatBadgeModule,
                ReactiveFormsModule,
                MatToolbarModule,
                MatMenuModule,
                MatDialogModule,
                MatGridListModule,
                MatTableModule,
                FormsModule,
                MatDatepickerModule,
                MatNativeDateModule,
            ],
            providers: [
                {provide: DateAdapter, useClass: MyDateAdapter},
                {provide: MAT_NATIVE_DATE_FORMATS, useValue: MY_DATE_FORMATS},
            ],
  }
)
export class MaterialModule { }

 