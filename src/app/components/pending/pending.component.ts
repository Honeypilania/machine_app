import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { DataSource } from '@angular/cdk/table';
import { MachineService } from '../../services/machine/machine.service';
import { AngularFireAuth } from '@angular/fire/auth';
 import { Observable, Subject, observable,combineLatest } from 'rxjs';
import { Observable } from 'rxjs';

import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-pending',
  templateUrl: './pending.component.html',
  styleUrls: ['./pending.component.scss']
})
export class PendingComponent implements OnInit, OnDestroy {
  //  //search code starts..
  searchterm: string;
  // //search code ends..
  d1;
  
  isMachine:boolean;
  isInsurance:boolean;
  isPermit:boolean;
  isRoadTax:boolean;
  displayedColumns: string[] = ['machinName', '250', '1K','6K'];
  dataSource = new Pending(this.afs);
  displayedColumns2: string[] = ['machinName','issue','expire','status'];
    dataSource2 = new MIData(this.afs);

    displayedColumns3: string[] = ['vehicleName','issue','expire','status'];
    dataSource3 = new VIData(this.afs);

    displayedColumns4: string[] = ['vehicleName','issue','expire','status'];
    dataSource4 = new VPData(this.afs);

  p;
  admin=false;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

    date_diff_indays = function(date1, date2) {
      var dt1 = new Date(date1);
     var  dt2 = new Date(date2);
      return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate()) ) /(1000 * 60 * 60 * 24));
      }
//  //search code starts..
      startAt = new Subject();
      endAt = new Subject();
     startobs = this.startAt.asObservable();
     endobs = this.endAt.asObservable();

//  //search code ends..

  constructor(private route:ActivatedRoute,private afs:AngularFirestore,private machine:MachineService,private email:AngularFireAuth,private breakpointObserver:BreakpointObserver) 
  { 
    afs.collection('user').valueChanges().subscribe(usr=>{
      for(let user of usr)
      {
        this.p=user;
        if(this.p.email==this.email.auth.currentUser.email)
        {
          if(this.p.admin)
          {
            this.admin=true;
          }
        }
      }
    });
    this.d1=machine.machineList;
  }
  giveDate(d)
  {
    var value;
    value=parseInt(d)
    return (new Date(value).toLocaleDateString())
  }
   valid(d)
  {
    if(this.date_diff_indays(new Date(),new Date(d))<=30)
      return true;
      else
        return false;
  }
  
  ngOnInit() 
  {
   // search code starts..
    observable.combineLatest(this.startobs,this.endobs).subscribe((value)=>{
      this.firequery(value[0],value[1]).subscribe((machineName)=>{
        this.machine = machineName;
      })
    })
  //   search code ends..
    this.route.paramMap.subscribe(arr=>
      {
        if (arr.get('id')=='service') this.isMachine=true;
        else if(arr.get('id')=='insurance') this.isInsurance=true;
        else if(arr.get('id')=='permit') this.isPermit=true;
        else if(arr.get('id')=='roadtax') this.isRoadTax=true;
      }
)
  }
//  //search code starts..
  search($event){
    let q = $event.target.value;
    this.startAt.next(q);
    this.endAt.next(q + "\uf8ff");
  }
  firequery(start, end){
    return this.afs.collection('machine', ref=>
    ref.limit(4).orderBy('machineName').startAt(start).endAt(end)).valueChanges();
  }
//  //search code ends..
  ngOnDestroy()
  {
    this.isMachine=this.isInsurance=this.isRoadTax=this.isPermit=false;
  }
  twoFifty(key)
 {
   this.machine.twoFifty2(key);
 }
 oneK(key)
 {
   this.machine.oneK2(key);
 }
 sixK(key)
 {
   this.machine.sixK2(key);
 }


  check2(key)
  {
    var status;
    var check;
    this.d1=this.machine.machineList
      for(let a of this.d1){
           if(a.$key==key){
            status=parseInt(a.twoFifty.counter);
            check=parseInt(a.twoFifty.check);
           }
      }
      if(status<check) return true;
            else return false;
    
  }
  check1(key)
  {
    var status;
    var check;
    this.d1=this.machine.machineList
      for(let a of this.d1){
           if(a.$key==key){
            status=parseInt(a.oneK.counter);
            check=parseInt(a.oneK.check);
           }
      }
      if(status<check) return true;
            else return false;
  }
  
  check6(key)
  {
    var status;
    var check;
    this.d1=this.machine.machineList
      for(let a of this.d1){
           if(a.$key==key){
            status=parseInt(a.sixK.counter);
            check=parseInt(a.sixK.check);
           }
      }
      if(status<check) return true;
            else return false;
  }
}


export class Pending extends DataSource<any>
{
  constructor(private afs:AngularFirestore){super()}
  connect()
  {
    return this.afs.collection('machine', ref => ref.where('allSerStatus', '==', false)).valueChanges();
  }
  disconnect()
  {

  }
}

export class VIData extends DataSource<any>
{
  constructor(private afs:AngularFirestore){super()}
  connect()
  {
    return this.afs.collection('vehicle', ref => ref.where('insrncStatus', '==', false)).valueChanges();
  }
  disconnect()
  {

  }
}

export class VPData extends DataSource<any>
{
  constructor(private afs:AngularFirestore){super()}
  connect()
  {
    return this.afs.collection('vehicle', ref => ref.where('permitStatus', '==', false)).valueChanges();
  }
  disconnect()
  {

  }
}


export class MIData extends DataSource<any>
{
  constructor(private afs:AngularFirestore){super()}
  connect()
  {
    return this.afs.collection('machine', ref => ref.where('insrncStatus', '==', false)).valueChanges();
  }
  disconnect()
  {

  }
}
