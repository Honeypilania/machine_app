import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataSource } from '@angular/cdk/table';
import { MachineService } from '../../services/machine/machine.service';
import { Machine } from '../../models/machine.model';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.scss']
})
export class ServiceComponent implements OnInit {
  public myData:any=[];
  public myData2:any=[];
  d1;
  d2;
  d3;

  isMachine: boolean;
  isInsurance:boolean;
  isPermit:boolean;
  isRoadTax: boolean;
  displayedColumns: string[] = ['machinName', '250', '1K','6K'];
  dataSource :Machine[];
  machineList= new MachineData(this.machine);

  constructor(private route:ActivatedRoute,private machine:MachineService) { 
     }

  ngOnInit() {
    
    this.route.paramMap.subscribe(arr=>
      {
        if (arr.get('id')=='machine') this.isMachine=true;
        else if(arr.get('id')=='insurance') this.isInsurance=true;
        else if(arr.get('id')=='permit') this.isPermit=true;
        else if(arr.get('id')=='roadtax') this.isRoadTax=true;
      }
);
  }

  check11(){
       while(this.machineList!=null) {
           var i=0;
          var status= parseInt(this.machineList[i].twoFifty.counter)
           if(status>=250){
                     this.dataSource.push(this.machineList[i]);
           }
           i++;
         }
  }

  check2(i)
  {
    this.d1=this.machine.machineList
    var status=parseInt(this.d1[i].twoFifty.counter)
    if(status>=250) return true;
    else return false;
  }
  check(i)
  {
    this.d1=this.machine.machineList
    var status=parseInt(this.d1[i].oneK.counter)
    if(status>=1000) return true;
    else return false;
  }
  check6(i)
  {
    this.d1=this.machine.machineList
    var status=parseInt(this.d1[i].sixK.counter)
    if(status>=6000) return true;
    else return false;
  }

}
export class MachineData extends DataSource<any>
{
  constructor(private machine:MachineService){super()}
  connect()
  {
    return this.machine.getValue();
  }
  disconnect()
  {

  }
}
