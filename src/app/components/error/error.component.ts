import { Component } from '@angular/core';

@Component({
  template:`<div class="main1">
  <div class="inner1">
    <div class="deep1">
      <div class="work1">
        <h1>Oops!!!!</h1>
        <h2>404 Not found</h2>
        <h3>Please make sure.....</h3>
        <span>1. Requested page is valid</span><br>
        <span>2. Network connection is stablished</span>
       <a href='' mat-stroked-button class="btn">Go to home</a> 
      </div>
    </div>
  </div>
</div>
<style>
.main1
{
    width: 100%;
    height: 100%;
    background-size: cover;
    display: table;
    background-repeat: no-repeat;
    top: 0;
    flex: 1 1 auto;
}
.inner1
{
    display:table-row;
    background-size: cover;
    background-repeat: no-repeat;
    width: 100%;
    height: 100%;
    top: 0;
}
.deep1
{
    display:table-cell;
    background-size: cover;
    background-repeat: no-repeat;
    vertical-align: middle;
    height: 100%;
    width:100%;
    top: 0;
    background-image: url(../../../assets/error.jpg)
}
.work1
{
    display: table;
    margin : auto auto;
    height: 400px;
    z-index: 1;
    border-radius: 10px;
    padding: 20px;
    color: white;
}
h1
{
    font-size: 50px;
}
h2
{
    font-size: 40px;
    color: red;
}
.btn
{
  height: auto;
  width: 100%;
  margin-top: 20px;
  border:2px solid white;
}
</style>`,
})
export class ErrorComponent {}