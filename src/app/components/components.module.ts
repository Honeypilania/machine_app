import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MaterialModule } from "./material";
import { RouterModule } from '@angular/router';
import { ErrorComponent } from './error/error.component';
import { DashComponent } from './dash/dash.component';
import { DetailsComponent } from './details/details.component';
import { PendingComponent } from './pending/pending.component';
import { EntriesComponent } from './entries/entries.component';
import { ServiceComponent } from './service/service.component';
import { IonicModule } from '@ionic/angular';
import { PermitComponent } from './permit/permit.component';
import { DialogComponent } from './dialog/dialog.component';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    MaterialModule,
    RouterModule,
    IonicModule
  ],
  declarations: 
                [
                  DashboardComponent, 
                  DashComponent, 
                  ErrorComponent,
                  DetailsComponent,
                  PendingComponent,
                  EntriesComponent,
                  ServiceComponent,
                  PermitComponent,
                  DialogComponent,
                  
                ]
})
export class ComponentsModule { }
