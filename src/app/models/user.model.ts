export interface Roles {
    admin?: boolean;
    supervisor?: boolean;
    master?:boolean;
  }
  
  export interface User{
    email: string;
    mobile: string;
    displayName: string;
    roles: Roles;
  
  }
