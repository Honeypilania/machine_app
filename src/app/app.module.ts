import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireModule } from "@angular/fire";
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DetailsComponent } from './components/details/details.component';
import { MachineService } from './services/machine/machine.service';
import { SimpleNotificationsModule } from "angular2-notifications";
import { ComponentsModule } from '../app/components/components.module';
import { MaterialModule } from './components/material';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { DialogComponent } from './components/dialog/dialog.component';
import { AngularFireDatabaseModule } from "@angular/fire/database";

// import all below for search functionality

import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [AppComponent],
  entryComponents: [DialogComponent],
  imports: [MaterialModule,ComponentsModule,AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule.enablePersistence(),
  
    FormsModule,
    AngularFireDatabaseModule,
      BrowserModule, IonicModule.forRoot(), AppRoutingModule, BrowserAnimationsModule,
      SimpleNotificationsModule.forRoot({position:["top","right"],timeOut:3000, maxStack:1,showProgressBar:false, clickToClose:true})],
  providers: [DetailsComponent,MachineService,LocalNotifications,
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
