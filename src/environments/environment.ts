// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDpdCyEgwBaIBogrqrdZqnPeFDeOtVY26Y",
  authDomain: "ac-machine-uat-b7aa7.firebaseapp.com",
  databaseURL: "https://ac-machine-uat-b7aa7.firebaseio.com",
  projectId: "ac-machine-uat-b7aa7",
  storageBucket: "ac-machine-uat-b7aa7.appspot.com",
  messagingSenderId: "295831451603",
  appId: "1:295831451603:web:440b1bee3a1518ad"
    
  }
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
