

.mainContainer
    {
        display: table;
        background-size: cover;
        width: 100%;
        height: 100%;
        top: 0;
        flex: 1 1 auto;
    }

	.holder
    {
        display:table-row;
        background-size: cover;
        background-repeat: no-repeat;
        width: 100%;
        height: 100%;
        top: 0;
    }

	.image-container
    {
        display:table-cell;
        background-image: url(../../../assets/image/final.jpg);
        background-size: cover;
        height: 100%;
        width:-450px;
        top: 0;
        }

		.login-container
    {
        display:table-cell;
        background-size: cover;
        background-repeat: no-repeat;
        vertical-align: middle;
        height: 100%;
        width: 450px;
        top: 0;
        margin-bottom: -15px;
        
    }
	
	.smartLogin
    {
      display: table;
      margin : auto auto;
      width: 350px;
    }
    @media(max-width:400px)
  {
    .smartLogin
    {
      display: table;
      margin :auto;
      width: 300px;
    }
  }
  
  .child-image
    {
      
      display: block;
      width: 100px;
      height: auto;
      overflow: hidden;
      margin: 0 auto;
    }
.padding
    {
        padding-left: 50px;
        padding-right: 50px;
        display: block
    }
  
	img
    { 
      width: 100%;
      position: relative;
    }
	
	span
    {
      font-family: Poppins-Regular;
      color: #979090;
      line-height: 1.5;
      font-size: 20px;
    }
	
	 .smartLogo1
    {
          font-family: 'Mrs Sheppards','cursive';
          font-size: 40px;
          margin-left: 10px;
          pointer-events: none;
          color: rgb(86, 179, 36);
          font-size: 50px;
    }
    .smartLogo2
    {
          font-family: 'Mrs Sheppards','cursive';
          margin-left: 10px;
          pointer-events: none;
          color:  rgb(25, 157, 209);
          font-size: 50px;
    }

	
mat-form-field
    {
      font-size:20px;
      font-family: Poppins-Bold;
    }
	
	.container 
    {
        display: flex;
        flex-direction: column;
        height:30px;
        width: 100%;
        position: relative;
        padding-bottom: 50px;
        
   }
   
   .form-button
    {
      width: 100%;
      height:45px;
      font-size:25px;
      border-radius: 25px;
      font-family: Poppins-Regular; 
      background-color: rgba(23, 87, 190, 0.986)
    }

   .form-button:hover:enabled  
    {
      background-color: #0ea709;
      box-shadow: 0 10px 30px 0px rgba(51, 51, 51, 0.5);
      -moz-box-shadow: 0 10px 30px 0px rgba(51, 51, 51, 0.5);
      -webkit-box-shadow: 0 10px 30px 0px rgba(51, 51, 51, 0.5);
      -o-box-shadow: 0 10px 30px 0px rgba(51, 51, 51, 0.5);
      -ms-box-shadow: 0 10px 30px 0px rgba(51, 51, 51, 0.5);
      width: 100%;
      font-size:25px;
      font-family: Poppins-Regular;   
      border-radius: 25px;
    }

	
mat-card-actions
    {
        font-family: Poppins-Regular;
        color: #999999;
        line-height: 1.5;
        font-size: 18px;
    }
	
	a
    {
      text-decoration: none;
    }

a:hover
    {
      color: #0ea709;
      box-shadow: 0 10px 30px 0px rgba(165, 170, 167, 0.411);
      line-height: 1.5;
    }



//give style to particular component ////////////////////////////////////////////////////////////////////////////////


#mat3
    {   
      padding-bottom: 20px;
      text-align: center;
      font-family: Poppins-Black;
      font-size: 39px;
      color: #333333;
      line-height: 1.2;
    }

	#td1:hover
    {
            -webkit-transform: scale(1.3);
            -ms-transform: scale(1.3);
            transform: scale(1.3);
    }
  //particular style class in orderd form///////////////////////////////////////////////////////////////////////////////////////






.component 
    {
      display: block;
      width: 400px;
      min-height: 30rem;
      margin: 5rem auto;
      background: #FFFFFF;
      position: relative;
    }



  

   